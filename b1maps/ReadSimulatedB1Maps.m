function B1perVpeak_ch = ReadSimulatedB1Maps(B1_fullpath,VoltageScaleFactor)
%% *************************************************************************
% FUNCTION:    B1perVpeak_ch = ReadSimulatedB1Maps(B1_fullpath,VoltageScaleFactor)
%
% DESCRIPTION: This function will scale B1 fields by the voltage scaling
% factor (as determined by CalcB1ScalingFactor.m). It also rotates the B1
% maps into radiological space used by the B1 shimming and SAR scripts
% maintained by KMG. It converts B1 maps to nT/Vpeak.
%
% INPUTS:      B1_fullpath - full path to 'B1.mat' file (i.e., B1+ maps)
%              VoltageScaleFactor - Voltage scale factor for each coil
%               element as created from CalcB1ScalingFactor.m (1 x nCoils)
%               [V]. If not specified, this will default to ones (1xnCoils)
%
% OUTPUTS:     B1perVpeak_ch - B1 matrix in nT/Vpeak, scaled by the voltage
%              scaling factor, and re-oriented.
%
% Created by:  Kyle M. Gilbert
%              June 2020 (KMG)
%
% Copyright:   Kyle M. Gilbert
%% ************************************************************************

%%
% Copyright 2020-2021 Kyle M. Gilbert
%
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the "Software"),
% to deal in the Software without restriction, including without limitation
% the rights to use, copy, modify, merge, publish, distribute, sublicense,
% and/or sell copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:
%
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
% THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
% FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
% DEALINGS IN THE SOFTWARE.
%%

% Read in the simulated B1+ maps
% Check to see if the B1 maps have already been rotated into the proper
% orientation and if they have been scaled appropriately. The convention is
% that the field maps are named 'Bz' directly out of CST and renamed to
% 'B1perVpeak_ch' once they have been scaled and re-orientated.
variableInfo = who('-file', B1_fullpath);
if ismember('B1perVpeak_ch', variableInfo)
   load(B1_fullpath, 'B1perVpeak_ch');
elseif ismember('Bz', variableInfo)
   load(B1_fullpath, 'Bz');

    if nargin < 2 || isempty(VoltageScaleFactor)
        VoltageScaleFactor = ones(1,size(Bz,4));
    end
    B1perVpeak_ch = ScaleAndRotateB1maps(Bz,VoltageScaleFactor);
end

end

%% Rotate to radiological space and scale maps to nT/Vpeak
function B1perVpeak_ch = ScaleAndRotateB1maps(Bz,VoltageScaleFactor)
% Imported B1+ maps are normalized, when exported, to 0.5-W accepted power.
% This is an average power, so the peak power is 1W. (P_peak = 2*P_avg)
% These maps correspond to zero phase in the simulated data

% Rotate the simulated maps to radiological space
B1perVpeak_ch = rot90(Bz,3);
B1perVpeak_ch = fliplr(B1perVpeak_ch);

% Scale simulated data from T/W_peak to T/Vpeak
B1perVpeak_ch = B1perVpeak_ch ./ sqrt(50); % Convert from T/W_peak to T/Vpeak
B1perVpeak_ch = B1perVpeak_ch .* 1e9;  % Convert from T/Vpeak to nT/Vpeak

% Scale maps by voltageScaleFactor if desired
for coil_i = 1:size(B1perVpeak_ch,4)
    % Apply magnitude scaling to the simulated data
    B1perVpeak_ch(:,:,:,coil_i) = B1perVpeak_ch(:,:,:,coil_i) .* VoltageScaleFactor(coil_i);
end

end
