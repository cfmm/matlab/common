function Password = get_token()

% get_token() for downloading files from gitlab

%%
% Copyright 2020-2021 L. Martyn Klassen
%
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the "Software"),
% to deal in the Software without restriction, including without limitation
% the rights to use, copy, modify, merge, publish, distribute, sublicense,
% and/or sell copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:
%
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
% THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
% FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
% DEALINGS IN THE SOFTWARE.
%%

%% Check for Existance of Java
if ~usejava('swing')
   error('passwordEntryDialog: Java is required for this program to run.');
end

%% Determine GUI Size and Position
% Center the GUI on the screen.

set(0,'Units','pixels')
Screen = get(0,'screensize');

PositionGUI = [0 0 255 40];

PositionGUI = [Screen(3:4)/2-PositionGUI(3:4)/2 PositionGUI(3:4)];
PositionLeft = 10;
BoxWidth = 200;

%% Create the GUI

BackgroundColor = get(0,'DefaultUicontrolBackgroundcolor');
handles.figure1 = figure('Menubar','none', ...
    'Units','Pixels', ...
    'Resize','off', ...
    'NumberTitle','off', ...
    'Name','Access Token', ...
    'Position',PositionGUI, ...
    'Color', BackgroundColor, ...
    'WindowStyle','modal');

% Create Token Entry Box
handles.java_Password = javax.swing.JPasswordField();
[handles.java_Password, handles.edit_Password] = javacomponent(handles.java_Password, [PositionLeft 10 BoxWidth 23], handles.figure1);
handles.java_Password.setFocusable(true);
set(handles.java_Password, 'ActionPerformedCallback', {@pushbutton_OK_Callback, handles});

set(handles.edit_Password, ...
    'Parent', handles.figure1, ...
    'Tag', 'edit_Password', ...
    'Units', 'Pixels', ...
    'Position',[PositionLeft 10 BoxWidth 23]);

 % Yes run it twice or it does not work
drawnow;
handles.java_Password.requestFocus;
drawnow;
handles.java_Password.requestFocus;

% Create OK Pushbutton
handles.pushbutton_OK = uicontrol('Parent',handles.figure1, ...
    'Tag', 'pushbutton_OK', ...
    'Style','Pushbutton', ...
    'Units','Pixels',...
    'Position',[PositionLeft+BoxWidth+5 10 30 23], ...
    'FontSize',10, ...
    'String','OK',...
    'HorizontalAlignment', 'Center',...
    'Callback', {@pushbutton_OK_Callback, handles},...
    'KeyPressFcn', {@pushbutton_KeyPressFcn, handles});

%% Wait for Completion

uiwait(handles.figure1);

if ishandle(handles.figure1)
    Password = handles.java_Password.Password';
    delete(handles.figure1);
else
    Password = -1;
end

end


function pushbutton_KeyPressFcn(hObject, eventdata, handles)

switch eventdata.Key
    case 'return'
        Callback = get(hObject, 'Callback');
        feval(Callback{1}, hObject, '', Callback{2:end});
end

end


function pushbutton_OK_Callback(hObject, eventdata, handles)
uiresume(handles.figure1)
end
