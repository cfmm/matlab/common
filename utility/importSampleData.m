function importSampleData(datadir,ZipFilename,UnzippedDirName,ProjectID,token)
% **************************************************************************
% FUNCTION:    importSampleData(datadir,ZipFilename,UnzippedDirName,ProjectID)
%
% DESCRIPTION: This function imports sample data from a gitlab project
%              The zipped file is unzipped after a gitlab personal token is
%              validated from the user. The zip file and extraneous files
%              are then deleted. If the folder exists, this process will be
%              skipped.
%
% INPUT:       datadir - directory name where data should be stored after
%               unzipping [str]
%              ZipFilename - name of zip file [str]
%              UnzippedDirName - unzipped folder name [str]
%              ProjectID - project ID, as found on gitlab project page
%              token - gitlab access token for project
%
% OUTPUT:      Folder of data that can be used for unit testing, etc.
%
% Copyright:   L. Martyn Klassen
%% ************************************************************************

%%
% Copyright 2020-2021 L. Martyn Klassen
%
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the "Software"),
% to deal in the Software without restriction, including without limitation
% the rights to use, copy, modify, merge, publish, distribute, sublicense,
% and/or sell copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:
%
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
% THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
% FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
% DEALINGS IN THE SOFTWARE.
%%
if nargin < 5
   token = [];
end

% full path of zip file
filename = [datadir ZipFilename];

% Only download the data if it is not already present in the datadir
if ~exist([datadir filesep UnzippedDirName], 'dir')

    gitlab_download(filename, ZipFilename, ProjectID, token);

    % unzip data
    unzip(filename, datadir);

    % delete the zip file
    delete(filename);

    % When unzipping the data on a mac through matlab, it can produce
    % this empty folder. If it exists, delete it.
    if exist([datadir '__MACOSX'],'dir')
        rmdir([datadir '__MACOSX'],'s');
    end

end

