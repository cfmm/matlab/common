function gitlab_download(destination,filename,ProjectID,token)
% **************************************************************************
% FUNCTION:    gitlab_download(destination,filename,ProjectID,token)
%
% DESCRIPTION: This function downloads filename from gitlab project with ProjectID
%              and save it to destination
%
% INPUT:       destination - name of file after download
%              filename - filename in gitlab project
%              ProjectID - project ID, as found on gitlab project page
%              token - Gitlab access token for project
%
% Copyright:   L. Martyn Klassen
%% ************************************************************************

%%
% Copyright 2020-2021 L. Martyn Klassen
%
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the "Software"),
% to deal in the Software without restriction, including without limitation
% the rights to use, copy, modify, merge, publish, distribute, sublicense,
% and/or sell copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:
%
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
% THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
% FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
% DEALINGS IN THE SOFTWARE.
%%

if isstring(ProjectID)
   ProjectID = int2str(char(ProjectID));
end

if ~ischar(ProjectID)
   ProjectID = int2str(ProjectID);
end

if nargin > 4 && ~isempty(token)
   options = weboptions('HeaderFields', {'PRIVATE-TOKEN' token}, 'Timeout', 20);
else
   options = weboptions('Timeout', 20);
end

% API url of where data is found
url = ['https://gitlab.com/api/v4/projects/' ProjectID '/repository/files/' urlencode(filename) '/raw?ref=master'];

try
   websave(destination, url, options);
catch
   % require a gitlab token
   token = get_token();
   if (token == -1)
      error('Invalid token')
   end

   % ask for a private gitlab token and increase the default timeout time
   options = weboptions('HeaderFields', {'PRIVATE-TOKEN' token}, 'Timeout', 20);

   try
      % save the data
      websave(destination, url, options);
   catch err
      delete(destination);
      rethrow(err);
   end
end

