function [roii, roimap] = getroi(mri, strTitle, shape)
%GETROI Select polygonal region of interest.
%   Modified from ROIPOLY
%
%  [roii, roimap] = getroi(image, title, shape)
%    roii   - index of pixels in roi
%    roimap - binary image of roi
%    image  - image used to select roi
%    title  - title to display on image
%    shape  - shape of roi to select: 'r', rectangle, [], free shaped
%             polygon

%%
% Copyright 2021 L. Martyn Klassen
%
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the "Software"),
% to deal in the Software without restriction, including without limitation
% the rights to use, copy, modify, merge, publish, distribute, sublicense,
% and/or sell copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:
%
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
% THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
% FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
% DEALINGS IN THE SOFTWARE.
%%

% Display the mri image
hfig = get(0,'CurrentFigure');
if isempty(hfig)
    hfig = figure;
else
    figure(hfig);
end

set(hfig, 'NumberTitle', 'off');

if nargin > 1
   if not(isempty(strTitle))
      set(hfig, 'Name', strTitle);
   else
      set(hfig, 'Name', 'Select ROI');
   end
else
    set(hfig, 'Name', 'Select ROI');
end

if ~isreal(mri)
   mri = abs(mri);
end

%colormap(gray(32))
colormap('default')
hold off;
imagesc(mri);
colorbar;
hold on;
colormap(gray);

axis image square manual;

[nrows, ncols] = size(mri);
xdata = [1 ncols];
ydata = [1 nrows];

found = 0;

if nargin < 3
   shape = 'p';
end

if strcmp(shape, 'r')
   % user could close the figure while you waiting
   % so use try ... catch
   try
      rect = getrect(hfig);
      found = 1;
   end

   xi = [rect(1) rect(1)+rect(3) rect(1)+rect(3) rect(1)         rect(1)];
   yi = [rect(2) rect(2)         rect(2)+rect(4) rect(2)+rect(4) rect(2)];
else
   try
      [xi,yi] = getline(hfig,'closed');
      found = 1;
   end
end

try
    set(hfig, 'NumberTitle', 'on');
    set(hfig, 'Name', '')
end

% Terminate if the user cancelled the selection process
if found ~= 1
   warning off
   return
end

% Transform xi,yi into pixel coordinates.
roix = axes2pix(ncols, xdata, xi);
roiy = axes2pix(nrows, ydata, yi);

% Round input vertices to the nearest 0.5 and then add 0.5.
roix = floor(roix + 1);
roiy = floor(roiy + 1);

% Initialize output matrix.  We need one extra row to begin with.
d = zeros(nrows+1,ncols);

num_segments = numel(roix) - 1;

% Process each segment.
for counter = 1:num_segments
  x1 = roix(counter);
  x2 = roix(counter+1);
  y1 = roiy(counter);
  y2 = roiy(counter+1);

  % We only have to do something with this segment if it is not vertical
  % or a single point.
  if (x1 ~= x2)

    % Compute an approximation to the segment drawn on an integer
    % grid.  Mark appropriate changes in the x direction in the
    % output image.
    [x,y] = intline(x1,x2,y1,y2);
    if ((x1 < 1) || (x1 > ncols) || (x2 < 1) || (x2 > ncols) || ...
        (y1 < 1) || (y1 > (nrows+1)) || (y2 < 1) || (y2 > (nrows+1)))
      xLowIdx = find(x < 1);
      if (~isempty(xLowIdx))
        x(xLowIdx) = ones(size(xLowIdx));
      end
      xHighIdx = find(x > ncols);
      if (~isempty(xHighIdx))
        x(xHighIdx) = (ncols+1) * ones(size(xHighIdx));
      end
      yLowIdx = find(y < 1);
      if (~isempty(yLowIdx))
        y(yLowIdx) = ones(size(yLowIdx));
      end
      yHighIdx = find(y > (nrows+1));
      if (~isempty(yHighIdx))
        y(yHighIdx) = (nrows+1) * ones(size(yHighIdx));
      end
    end
    diffx = diff(x);
    dx_indices = find(diffx);
    dx_indices = dx_indices(:);  % converts [] to rectangular empty;
                                 % helps code below work for degenerate case
    if (x2 > x1)
      mark_val = 1;
    else
      mark_val = -1;
      dx_indices = dx_indices + 1;
    end
    d_indices = [y(dx_indices) (x(dx_indices)-1)] * [1; (nrows+1)];
    d(d_indices) = d(d_indices) + mark_val(ones(size(d_indices)),1);
  end

end

% Now a cumulative sum down the columns will fill the region with
% either 1's or -1's.  Compare the result with 0 to force a
% logical output.
d = (cumsum(d) ~= 0);

% Get rid of that extra row and we're done!
d(end,:) = [];

roimap = d;
roii = find(d == 1);

plot(roix, roiy, 'r');

hold off;
return



function pixelx = axes2pix(dim, x, axesx)
%AXES2PIX Convert axes coordinates to pixel coordinates.

xfirst = x(1);
xlast = x(max(size(x)));

if (dim == 1)
  pixelx = axesx - xfirst + 1;
  return;
end
xslope = (dim - 1) / (xlast - xfirst);
if ((xslope == 1) && (xfirst == 1))
  pixelx = axesx;
else
  pixelx = xslope * (axesx - xfirst) + 1;
end
return




function [x,y] = intline(x1, x2, y1, y2)
%INTLINE Integer-coordinate line drawing algorithm.

dx = abs(x2 - x1);
dy = abs(y2 - y1);

% Check for degenerate case.
if ((dx == 0) && (dy == 0))
  x = x1;
  y = y1;
  return;
end

flip = 0;
if (dx >= dy)
  if (x1 > x2)
    % Always "draw" from left to right.
    t = x1; x1 = x2; x2 = t;
    t = y1; y1 = y2; y2 = t;
    flip = 1;
  end
  m = (y2 - y1)/(x2 - x1);
  x = (x1:x2).';
  y = round(y1 + m*(x - x1));
else
  if (y1 > y2)
    % Always "draw" from bottom to top.
    t = x1; x1 = x2; x2 = t;
    t = y1; y1 = y2; y2 = t;
    flip = 1;
  end
  m = (x2 - x1)/(y2 - y1);
  y = (y1:y2).';
  x = round(x1 + m*(y - y1));
end

if (flip)
  x = flipud(x);
  y = flipud(y);
end
return
