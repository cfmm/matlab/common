function data = fft3d(data)

% FFT3D Forward Fourier transform on first three dimensions
%   X = FFT3D(DATA) perfroms the forward Fourier transform on the first three
%   dimensions of centrically ordered data. Result is centrically ordered.

%%
% Copyright 2020-2021 L. Martyn Klassen
%
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the "Software"),
% to deal in the Software without restriction, including without limitation
% the rights to use, copy, modify, merge, publish, distribute, sublicense,
% and/or sell copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:
%
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
% THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
% FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
% DEALINGS IN THE SOFTWARE.
%%

[m, n, o, p] = size(data);
siz = size(data);
r1 = floor(m/2);
r2 = floor(n/2);
r3 = floor(o/2);

data = data([r1:-1:1 m:-1:r1+1], [r2:-1:1 n:-1:r2+1], [r3:-1:1 o:-1:r3+1],:);
for q = 1:p
    data(:,:,:,q) = fftn(data(:,:,:,q));
end
r1 = ceil(m/2);
r2 = ceil(n/2);
r3 = ceil(o/2);

data = data([r1:-1:1 m:-1:r1+1], [r2:-1:1 n:-1:r2+1], [r3:-1:1 o:-1:r3+1],:);
data = reshape(data, siz);
return
