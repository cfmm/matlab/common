function [roi] = getroi3d(p1, p2)

% GETROI3D Graphical Select of ROI within 3D image

%%
% Copyright 2021 L. Martyn Klassen
%
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the "Software"),
% to deal in the Software without restriction, including without limitation
% the rights to use, copy, modify, merge, publish, distribute, sublicense,
% and/or sell copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:
%
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
% THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
% FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
% DEALINGS IN THE SOFTWARE.
%%

% WARNING: This is a rather complex function which makes extensice use
% of callbacks and graphical user interface objects.

if nargin < 1
   error('GETROI3D requires one argument.');
end

% Check to see if the function is being called as a callback
% or to initially start the program. Callbacks ALL have text
% string arguments, while the initial data is a 3D image matrix
% of doubles.
if isa(p1,'double')
   % Display only works on real data so complex data is first
   % converted to magnitude values
   if ~isreal(p1)
      p1 = abs(p1);
   end

   % Record the size of 3D image
   [d.Ydim, d.Xdim, d.Zdim] = size(p1);

   % d.roi stores a 3D binary matrix indicating if a voxel is
   % within the selected roi
   d.roi = zeros(size(p1));

   % The starting roi is the voxels indexed by the second argument
   % or nothing at all
   d.default = [];
   if nargin > 1
      d.roi(p2) = 1;
      d.default = p2;
   end

   % d.map stores colormap that has gray scale in the first
   % 256 entries, and a red scale in the second 256 entries
   % This is used to display images with red tinge on pixels
   % within the select ROI.
   d.map = [gray(256); ((71:326)'./326) zeros(256,2)];

   % d.original stores the original data
   d.original = p1;

   % d.lim stores the minimum and maximum values with the image
   d.lim = [min(p1(:)) max(p1(:))];

   if d.lim(1) == d.lim(2);
      d.lim(1) = d.lim(1) - 1e6*eps;
      d.lim(2) = d.lim(2) + 1e6*eps;
   end

   % d.data stores the image rescaled to 1 to 256
   d.data = round((p1 - d.lim(1))./(d.lim(2) - d.lim(1)).*255 + 1);

   % d.current stores the current slice to display
   d.current = 1;

   % d.fig stores the handle to a newly created figure used in display
   d.fig = figure('Name', 'Select ROI', 'toolbar',...
      'none', 'NumberTitle', 'off','MenuBar','none', 'visible', 'off',...
      'units', 'character');
   temp = get(d.fig, 'position');
   set(d.fig, 'position', [temp(1:3), 30]);

   % Create a listbox that displays all the slices in the 3rd direction
   d.txtSlice = uicontrol('style','text','string','Slice','units','character');
   slicelist = cell(1,d.Zdim);
   for n = 1:d.Zdim
      slicelist{n} = num2str(n);
   end
   d.slice = uicontrol('style','listbox','string',...
	    slicelist,'callback','getroi3d slice','units','character');

   % Create a button that indicates removal or addition to ROI
   d.remove = uicontrol('style','checkbox','string','Remove',...
      'units','character','max',1, 'min',0);

   % Create a listbox for choosing the type of ROI selection to use
   d.mode = uicontrol('style','popup','string',...
      {'Selection Mode','points','rectangle','polygon','freeform'},...
	    'callback','getroi3d mode','units','character');

   % Create an automatic threshold selection button
   d.string = uicontrol('style', 'text','string','Threshold Select', ...
      'units','character','horizontalalignment','left');
   d.set = uicontrol('style', 'pushbutton','string','Select', ...
      'callback', 'getroi3d set','units','character');
   d.value = 0.2;
   d.threshold = uicontrol('style', 'edit','string',num2str(d.value), ...
      'callback', 'getroi3d threshold','units','character','horizontalalignment','right',...
      'Interruptible', 'off');

   % Create a button to click to terminate ROI selection
   % and return the selected ROI
   d.done = uicontrol('style','pushbutton','string','Done',...
       'callback', 'getroi3d done','units','character');
   % and return a null ROI
   d.cancel = uicontrol('style','pushbutton','string','Cancel',...
       'callback', 'getroi3d cancel','units','character');

   % Create a button to clear selected voxels from the ROI
   % in the current slice
   d.clearslice = uicontrol('style','pushbutton','string','Clear Slice',...
       'callback', 'getroi3d clearslice','units','character');
   % in entire 3D image data set
   d.clearall = uicontrol('style','pushbutton','string','Clear All',...
       'callback', 'getroi3d clearall','units','character');

   % Create a button to invert the selection
   d.invert = uicontrol('style','pushbutton','string','Invert',...
       'callback', 'getroi3d invert','units','character');

   % Template the current slice to all slices
   d.template = uicontrol('style','pushbutton','string','Template',...
      'callback', 'getroi3d template','units','character');

   % Set the callback functions for
   % 1) Clicking on the figure
   % 2) Resizing the figure
   % Also set the default background color, make sure
   % the settings hold, and set the colormap
   set(d.fig,'Color',get(0,'defaultUicontrolBackgroundColor'),...
      'WindowButtonUpFcn', 'getroi3d intensity',...
      'ResizeFcn', 'getroi3d resize', ...
      'NextPlot', 'add', 'Colormap', d.map);

   % Create an axes object on which the image will be displayed
   % Turn on the box to limit display to 2D plane
   % Supress tick marks and labels.
   % Reverse the Y direction
   % Measure sizes are to be reported in terms of characters
   d.axes = axes( 'box','on',...
        'Xtick', [], 'YTick', [],...
        'XTickLabel', [], 'YTickLabel', [],...
        'YDir','reverse',...
        'units', 'character', ...
        'NextPlot','replacechildren');

   % Display the current image
   image(d.data(:,:,d.current));

   % Set the image axis limits to image display setting
   axis image;

   % Call the resize function to preform the alignment and sizing work
   resize(d);

   % Store application data for retrieval in callbacks
   setappdata(d.fig,'getroi3d',d);

   % If the user destroys the figure while in a callback, we should
   % still have the return variable ROI defined (as a null matrix)
   roi = d.roi;

   % Wait for user input
   set(d.fig, 'Visible', 'on', 'waitstatus', 'waiting');
   waitfor(d.fig, 'waitstatus', 'inactive');

   % After completing the callback, return the latest ROI
   try
      d = getappdata(gcf,'getroi3d');
      roi = d.roi;
      delete(d.fig);
   end
else
   % This is used to process Callbacks

   [obj, fig] = gcbo;

   % Retrieve stored application data
   d=getappdata(fig,'getroi3d');

   % Interpret Callback commands
   try
      switch p1
      case 'cancel'
         % Set d.roi to null matrix
         d.roi = zeros(size(d.data));
         d.roi(d.default) = 1;
         % Turn off waiting
         set (d.fig, 'waitstatus', 'inactive');
      case 'slice'
         % Change d.current to the values selected in the listbox
         d.current = get(d.slice,'value');
         % Display the new slice
         showslice(d);
         % Reset the selection mode to NOTHING
         set(d.mode,'value',1);
      case 'done'
         % Turn off waiting
         set (d.fig, 'waitstatus', 'inactive');
      case 'mode'
         % Turn off the default callback function for clicking in figure
         set(d.fig,'WindowButtonUpFcn', '');
         % Pass the work of recording the new matrix to getroi (local function)
         d = getroi(d, get(gcbo, 'value'));
         % Display the slice to turn on the red highlighting of the roi
         showslice(d);
         % Turn on the default callback function for clicking in figure
         set(d.fig,'WindowButtonUpFcn', 'getroi3d intensity');
         % Reset the selection mode to NOTHING
         set(d.mode, 'value', 1);
      case 'clearslice'
         % Set the d.roi values for current slice to zero
         d.roi(:,:,d.current) = zeros(d.Ydim, d.Xdim);
         % Update display of the slice to remove ROI highlighting
         showslice(d);
      case 'clearall'
         % Reset d.roi to all zeros
         d.roi = zeros(size(d.data));
         % Update display to remove ROI highlighting
         showslice(d);
      case 'intensity'
         % This is the default behaviour of the figure when the mouse is clicked
         % It functions to adjust the display intensity so that the current pixel
         % intensity is the maximum

         % Get the current location
         pt = floor(get(d.axes,'CurrentPoint')+0.5);
         x = pt(1,1);
         y = pt(1,2);

         % Preform scaling and redisplay of the image
         if ((x > 0) && (y > 0) && (x <= d.Xdim) && (y <= d.Ydim))
            maxint = d.original(y,x,d.current);
            d.data = round((d.original - d.lim(1))./(maxint - d.lim(1)).*255 + 1);
            d.data(d.data>256) = 256;
            showslice(d);
         end
      case 'resize'
         % This is the default behaviour of the figure when it is resized
         resize(d);
      case 'threshold'
         newval = str2double(get(d.threshold,'string'));
         if numel(newval) ~= 1
            set(d.threshold, 'String', num2str(d.value));
         else
            d.value = newval;
         end
      case 'set'
         d.roi = (d.roi + (d.original > d.lim(2)*d.value)) > 0;
         showslice(d);
      case 'invert'
         d.roi = ~d.roi;
         showslice(d);
      case 'template'
         d.roi = d.roi(:,:,d.current.*ones(d.Zdim, 1));
         showslice(d);
      otherwise
         % Passing anything else is severely punished
         error('GETROI3D does not recognize callback argument %s', p1);
      end
   catch
      % Because this error occured during Callback, let the main program
      % continue to run. Just display the error message
      rethrow(lasterror);
   end

   % Save the application data since it has likely been changed
   setappdata(gcf,'getroi3d',d)
end
return

%------------------------ Local Work Horse Functions ---------------------------


function resize(d)
% Update the alignment and size of display elements when the
% user resizes the display window

% Get the figure size and position
set(d.fig, 'Units', 'character');
figsize = get(d.fig,'Position');

% If the resized figure is smaller than the minimum figure size
% then compensate by setting to the minimum
if figsize(3) < 85
   % If the width is too small then reset to minimum width
   set(d.fig,'Position',[figsize(1) figsize(2) 85 figsize(4)])
end
if figsize(4) < 32
   % If the height is too small then reset to minimum height
   set(d.fig, 'Position', [figsize(1), figsize(2)+figsize(4)-32, figsize(3), 32])
end
% Get the new size and position
figsize = get(d.fig,'Position');

% Position the display elements correctly given the new size
set(d.txtSlice, 'Position', [0 figsize(4)-1 22 1]);
set(d.slice, 'Position', [1 figsize(4)-11 20 10]);
set(d.remove, 'Position', [1 figsize(4)-12.5 20, 1.36]);
set(d.mode, 'Position', [1 figsize(4)-14 20 1.36]);
set(d.string, 'Position', [1 figsize(4)-15.4 20 1]);
set(d.threshold, 'Position', [1 figsize(4)-16.85 7 1.36]);
set(d.set, 'Position', [9 figsize(4)-17 12 1.6]);
set(d.clearslice, 'Position', [4 figsize(4)-19.5 14 1.6]);
set(d.clearall, 'Position', [4 figsize(4)-21.5 14 1.6]);
set(d.invert, 'Position', [4 figsize(4)-23.5 14 1.6]);
set(d.template, 'Position', [4 figsize(4)-25.5 14 1.6]);
set(d.done, 'Position', [4 figsize(4)-28 14 1.6]);
set(d.cancel, 'Position', [4 figsize(4)-30 14 1.6]);


% Position the axes top right corner
axissize = get(d.axes, 'Position');
set(d.axes, 'Position', [24 1  axissize(3) axissize(4)]);

% Get the figure and axes size in pixels instead of characters
set(d.fig, 'Units', 'pixels');
set(d.axes, 'Units', 'pixels');
figsize = get(d.fig,'Position');
axissize = get(d.axes, 'Position');

% Position the figure optimally within the available space
if (figsize(4) - 2*axissize(2)) > (figsize(3) - axissize(2) - axissize(1))
   axissize(3:4) = [(figsize(3) - axissize(2) - axissize(1)) (figsize(3) - axissize(2) - axissize(1))];
else
   axissize(3:4) = [(figsize(4) - 2.*axissize(2)) (figsize(4) - 2.*axissize(2))];
end
axissize(2) = figsize(4) - axissize(2) - axissize(4);
set(d.axes, 'Position', axissize);

% Switch size information back to characters
set(d.fig, 'Units', 'character');
set(d.axes, 'Units', 'character');
return



function showslice(d)
% Display the current slice with ROI highlighting
image(d.data(:,:,d.current)+d.roi(:,:,d.current).*256);
return



function d = getroi(a, shape)

% Define ROI as not changed in case function terminates
% prematurely
d = a;

% Define the size of the image slice
nrows = d.Ydim;
ncols = d.Xdim;

% If no selection shape is defined use shape 5
if nargin < 2
   shape = 5;
end

switch shape
case 1
   %Don't get anything
   return
case 2
   % Add selected points to ROI
   try
      [roix, roiy, but] = ginput(1);
      while (but < 2 && roix > 0.5 && roiy > 0.5 && roix < (ncols+0.5) && roiy < (nrows+0.5))
         roix = floor(roix+0.5);
         roiy = floor(roiy+0.5);
         if get(d.remove,'value') == 1
            d.roi(roiy,roix,d.current) = 0;
         else
            d.roi(roiy,roix,d.current) = 1;
         end
         showslice(d);
         [roix, roiy, but] = ginput(1);
      end
   end

   % This one is so simple no further processing is needed
   return
case 3
   % Select a rectangle

   % user could close the figure while you waiting
   % so use try ... catch
   try
      rect = getrect(d.fig);
   catch
      return
   end
   rect = floor(rect + 0.5);
   roix = max(rect(1), 1):min(rect(1)+rect(3), ncols);
   roiy = max(rect(2), 1):min(rect(2)+rect(4), nrows);
   if get(d.remove,'value') == 1
      d.roi(roiy,roix,d.current) = 0;
   else
      d.roi(roiy,roix,d.current) = 1;
   end

   % Again, this one is so simple no further processing is needed
   return
case 4
   % Select a closed polygon, one vertices per mouse click
   try
      [x,y] = getline(d.fig,'closed');
   catch
      rethrow(lasterror);
      return
   end

otherwise
   % Select a freeform shape, i.e. draw out the shape with the mouse
   try
      [x,y] = getshape(d.fig);
   catch
      rethrow(lasterror);
      return
   end
end

% The complex shapes (4+) require some post processing to convert
% the information into friendly terms.

roix = floor(x + 1);
roiy = floor(y + 1);

% Initialize output matrix.  We need one extra row to begin with.
mask  = zeros(nrows+1,ncols);

num_segments = numel(roix) - 1;

% Process each segment.
for counter = 1:num_segments
   x1 = roix(counter);
   x2 = roix(counter+1);
   y1 = roiy(counter);
   y2 = roiy(counter+1);

   % We only have to do something with this segment if it is not vertical
   % or a single point.
   if (x1 ~= x2)
      % Compute an approximation to the segment drawn on an integer
      % grid.  Mark appropriate changes in the x direction in the
      % output image.
      [x,y] = intline(x1,x2,y1,y2);
      if ((x1 < 1) || (x1 > ncols) || (x2 < 1) || (x2 > ncols) || ...
          (y1 < 1) || (y1 > (nrows+1)) || (y2 < 1) || (y2 > (nrows+1)))
         xLowIdx = find(x < 1);
         if (~isempty(xLowIdx))
            x(xLowIdx) = ones(size(xLowIdx));
         end
         xHighIdx = find(x > ncols);
         if (~isempty(xHighIdx))
            x(xHighIdx) = (ncols+1) * ones(size(xHighIdx));
         end
         yLowIdx = find(y < 1);
         if (~isempty(yLowIdx))
            y(yLowIdx) = ones(size(yLowIdx));
         end
         yHighIdx = find(y > (nrows+1));
         if (~isempty(yHighIdx))
            y(yHighIdx) = (nrows+1) * ones(size(yHighIdx));
         end
      end
      idx = find(diff(x));
      idx = idx(:);  % converts [] to rectangular empty;
                     % helps code below work for degenerate case

      if (x2 > x1)
         mark_val = 1;
      else
         mark_val = -1;
         idx = idx + 1;
      end
      idx = [y(idx) (x(idx)-1)] * [1; (nrows+1)];
      mask(idx) = mask(idx) + mark_val(ones(size(idx)),1);
   end

end

% Now a cumulative sum down the columns will fill the region with
% either 1's or -1's.  Compare the result with 0 to force a
% logical output.
mask = (cumsum(mask) ~= 0);
mask(end,:) = [];

if get(d.remove, 'value') == 1
   d.roi(:,:,d.current) = (d.roi(:,:,d.current) - mask ) > 0;
else
   d.roi(:,:,d.current) = (d.roi(:,:,d.current) + mask ) > 0;
end

return



function [x,y] = intline(x1, x2, y1, y2)
% Integer-coordinate line drawing algorithm.
dx = abs(x2 - x1);
dy = abs(y2 - y1);

incr = 1;
if (dx >= dy)
   if (x1 > x2)
      incr = -1;
   end
   m = (y2 - y1)/(x2 - x1);
   x = (x1:incr:x2).';
   y = round(y1 + m*(x - x1));
else
   if (y1 > y2)
      incr = -1;
   end
   m = (x2 - x1)/(y2 - y1);
   y = (y1:incr:y2).';
   x = round(x1 + m*(y - y1));
end
return
