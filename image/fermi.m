function B = fermi(A, N, cutoff, transition)
%
% FERMI apply fermi filter
%    B = FERMI(A, N, CUTOFF, TRANSITION) applies an ellipsoidal fermi filter in
%    the first N dimensions. The Fermi filter design is
%
%        filter = 1 / (1 + exp((r^2 - cutoff)./transition))
%
%    where r is position scaled from -1 to 1 over all dimensions. Different
%    CUTOFF and TRANSITION variables for each direction can be specified by
%    providing length N vectors.

%%
% Copyright 2004-2021 L. Martyn Klassen
%
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the "Software"),
% to deal in the Software without restriction, including without limitation
% the rights to use, copy, modify, merge, publish, distribute, sublicense,
% and/or sell copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:
%
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
% THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
% FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
% DEALINGS IN THE SOFTWARE.
%%

if nargin < 4
   transition = 0.02;
   if nargin < 3
      cutoff = 0.9;
      if nargin < 2
         N = [];
         if nargin < 1
            error('CFMM:fermi:Arguments':'FERMI requires one input argument');
         end
      end
   end
end

if isempty(N) || (N > ndims(A))
   N = ndims(A);
end

if any(transition <= 0)
   error('CFMM:fermi:Transition','FERMI requires transition greater than zero');
end

% Convert into inverse transition
expand = 1./transition;

if any(transition > 1)
   warning('CFMM:fermi:Deprecated','FERMI uses transition width, not expansion (1/transition)')
end

% Check and setup the variables
doN = false;
if numel(expand) == N
   doN = true;
elseif numel(expand) ~= 1
   error('CFMM:fermi:Transition','FERMI transition argument should be %d vector or scalar.', N);
else
   expand = expand(ones(1,N));
end

if numel(cutoff) == N
   doN = true;
elseif numel(cutoff) ~= 1
   error('CFMM:fermi:Cutoff','FERMI cutoff argument should be %d vector or scalar.', N);
elseif doN
   cutoff = cutoff(ones(1,N));
end

% Compute the position
siz = size(A);

if doN
   filter = ones([siz(1:N) 1]);
   for i = 1:N
      x = (-1:2/(siz(i)-1):1).^2;
      x = permute(x, [3:(i+1) 2  1]);
      filter = filter.*(1+exp(repmat(x, [siz(1:i-1) 1 siz(i+1:N)]) - cutoff(i)).*expand(i));
   end
   filter = 1./filter;
else
   expand = expand(1);
	% Ellipsoidal Fermi Filter
   filter = zeros([siz(1:N) 1]);
	for i = 1:N
      x = (-1:2/(siz(i)-1):1).^2;
      x = permute(x, [3:(i+1) 2  1]);
      filter = filter + repmat(x, [siz(1:i-1) 1 siz(i+1:N)]);
	end
	filter = 1./(1+exp((filter - cutoff).*expand));
end

B = A.*repmat(filter, [ones(1,N) siz(N+1:end)]);
