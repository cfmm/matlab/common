function [x, y] = getshape(varargin)
%GETSHAPE Select freeform shape with mouse.
%   [X, Y] = GETSHAPE(FIG) lets you select a freeform shape in the
%   current axes of figure FIG using the mouse.  Use the mouse to
%   click and drag the desired shape.  X and Y hold the coordinates
%   of the vertices of the shape.
%
%   [X, Y] = GETSHAPE(AX) lets you select a shape in the axes
%   specified by the handle AX.
%
%   See also GETRECT, GETLINE, GETPTS.

%   Callback syntaxes:
%        getshape('ButtonDown')
%        getshape('ButtonMotion')
%        getshape('ButtonUp')

%%
% Copyright 2021 L. Martyn Klassen
%
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the "Software"),
% to deal in the Software without restriction, including without limitation
% the rights to use, copy, modify, merge, publish, distribute, sublicense,
% and/or sell copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:
%
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
% THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
% FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
% DEALINGS IN THE SOFTWARE.
%%

global GETSHAPE_FIG GETSHAPE_AX GETSHAPE_H1 GETSHAPE_H2

if ((nargin >= 1) && (ischar(varargin{1})))
    % Callback invocation: 'ButtonDown', 'ButtonMotion', or 'ButtonUp'
    feval(varargin{:});
    return;
end

if (nargin < 1)
    GETSHAPE_AX = gca;
    GETSHAPE_FIG = get(GETSHAPE_AX, 'Parent');
else
    if (~ishandle(varargin{1}))
        error('First argument is not a valid handle');
    end

    switch get(varargin{1}, 'Type')
    case 'figure'
        GETSHAPE_FIG = varargin{1};
        GETSHAPE_AX = get(GETSHAPE_FIG, 'CurrentAxes');
        if (isempty(GETSHAPE_AX))
            GETSHAPE_AX = axes('Parent', GETSHAPE_FIG);
        end

    case 'axes'
        GETSHAPE_AX = varargin{1};
        GETSHAPE_FIG = get(GETSHAPE_AX, 'Parent');

    otherwise
        error('First argument should be a figure or axes handle');

    end
end

% Remember initial figure state
old_db = get(GETSHAPE_FIG, 'DoubleBuffer');
state = uisuspend(GETSHAPE_FIG);

% Set up initial callbacks for initial stage
set(GETSHAPE_FIG, ...
    'Pointer', 'crosshair', ...
    'WindowButtonDownFcn', 'getshape(''ButtonDown'');', ...
    'DoubleBuffer', 'on');

% Bring target figure forward
figure(GETSHAPE_FIG);

% Initialize the lines to be used for the drag
GETSHAPE_H1 = line('Parent', GETSHAPE_AX, ...
                  'XData', [0; 0], ...
                  'YData', [0; 0], ...
                  'Visible', 'off', ...
                  'Clipping', 'off', ...
                  'Color', 'k', ...
                  'LineStyle', '-');

GETSHAPE_H2 = line('Parent', GETSHAPE_AX, ...
                  'XData', [0; 0], ...
                  'YData', [0; 0], ...
                  'Visible', 'off', ...
                  'Clipping', 'off', ...
                  'Color', 'w', ...
                  'LineStyle', ':');


% We're ready; wait for the user to do the drag
% Wrap the waitfor call in try-catch so
% that if the user Ctrl-C's we get a chance to
% clean up the figure.
errCatch = 0;
try
    waitfor(GETSHAPE_H1, 'UserData', 'Completed');
catch
    errCatch = 1;
end

% After the waitfor, if GETSHAPE_H1 is still valid
% and its UserData is 'Completed', then the user
% completed the drag.  If not, the user interrupted
% the action somehow, perhaps by a Ctrl-C in the
% command window or by closing the figure.

if (errCatch == 1)
    errStatus = 'trap';

elseif (~ishandle(GETSHAPE_H1) || ...
            ~strcmp(get(GETSHAPE_H1, 'UserData'), 'Completed'))
    errStatus = 'unknown';

else
    errStatus = 'ok';
    x = get(GETSHAPE_H1, 'XData');
    y = get(GETSHAPE_H1, 'YData');
end

% Delete the animation objects
if (ishandle(GETSHAPE_H1))
    delete(GETSHAPE_H1);
end
if (ishandle(GETSHAPE_H2))
    delete(GETSHAPE_H2);
end

% Restore the figure state
if (ishandle(GETSHAPE_FIG))
   uirestore(state);
   set(GETSHAPE_FIG, 'DoubleBuffer', old_db);
end

% Clean up the global workspace
clear global GETSHAPE_FIG GETSHAPE_AX GETSHAPE_H1 GETSHAPE_H2
clear global GETSHAPE_PT1 GETSHAPE_DATA

% Depending on the error status, return the answer or generate
% an error message.
switch errStatus
case 'trap'
    % An error was trapped during the waitfor
    error('Interruption during mouse selection.');

case 'unknown'
    % User did something to cause the rectangle drag to
    % terminate abnormally.  For example, we would get here
    % if the user closed the figure in the drag.
    error('Interruption during mouse selection.');
end

%--------------------------------------------------
% Subfunction ButtonDown
%--------------------------------------------------
function ButtonDown %#ok<DEFNU>

global GETSHAPE_FIG GETSHAPE_AX GETSHAPE_H1 GETSHAPE_H2
global GETSHAPE_PT1 GETSHAPE_DATA

set(GETSHAPE_FIG, 'Interruptible', 'off', ...
                 'BusyAction', 'cancel');

pt = round(get(GETSHAPE_AX, 'CurrentPoint'));
x1 = pt(1,1);
y1 = pt(1,2);
GETSHAPE_DATA = [x1 y1; x1 y1];
GETSHAPE_PT1 = [x1 y1];

set(GETSHAPE_H1, 'XData', GETSHAPE_DATA(:,1), ...
                'YData', GETSHAPE_DATA(:,2), ...
                'Visible', 'on');
set(GETSHAPE_H2, 'XData', GETSHAPE_DATA(:,1), ...
                'YData', GETSHAPE_DATA(:,2), ...
                'Visible', 'on');

% Let the motion functions take over.
set(GETSHAPE_FIG, 'WindowButtonMotionFcn', 'getshape(''ButtonMotion'');', ...
                 'WindowButtonUpFcn', 'getshape(''ButtonUp'');');


%-------------------------------------------------
% Subfunction ButtonMotion
%-------------------------------------------------
function ButtonMotion %#ok<DEFNU>

global GETSHAPE_AX GETSHAPE_H1 GETSHAPE_H2
global GETSHAPE_PT1 GETSHAPE_DATA

pt = get(GETSHAPE_AX, 'CurrentPoint');
x = pt(1,1);
y = pt(1,2);
pt = [x y];

if (sum(GETSHAPE_DATA(end-1,:) ~= pt))
   GETSHAPE_DATA(end, :) = pt;
   GETSHAPE_DATA(end+1, :) = GETSHAPE_PT1;

   set(GETSHAPE_H1, 'XData', GETSHAPE_DATA(:,1), ...
                   'YData', GETSHAPE_DATA(:,2));
   set(GETSHAPE_H2, 'XData', GETSHAPE_DATA(:,1), ...
                   'YData', GETSHAPE_DATA(:,2));
end


%--------------------------------------------------
% Subfunction ButtonUp
%--------------------------------------------------
function ButtonUp %#ok<DEFNU>

global GETSHAPE_FIG GETSHAPE_AX GETSHAPE_H1 GETSHAPE_H2
global GETSHAPE_PT1 GETSHAPE_DATA

% Kill the motion function and discard pending events
set(GETSHAPE_FIG, 'WindowButtonMotionFcn', '', ...
                 'Interruptible', 'off');

% Set final line data
pt = round(get(GETSHAPE_AX, 'CurrentPoint'));
x = pt(1,1);
y = pt(1,2);
pt = [x y];

if (sum(GETSHAPE_DATA(end-1,:) ~= pt))
   GETSHAPE_DATA(end, :) = pt;
   GETSHAPE_DATA(end+1, :) = GETSHAPE_PT1;

   set(GETSHAPE_H1, 'XData', GETSHAPE_DATA(:,1), ...
                   'YData', GETSHAPE_DATA(:,2));
   set(GETSHAPE_H2, 'XData', GETSHAPE_DATA(:,1), ...
                   'YData', GETSHAPE_DATA(:,2));
end

% Unblock execution of the main routine
set(GETSHAPE_H1, 'UserData', 'Completed');
