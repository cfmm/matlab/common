function clean = despike(dirty, tol)

% newimages = DESPIKE(images, tol)
%
% DESPIKE locates all points that are tol times larger than the average of the
% point and its nearest neighbors and replaces it with the average of the nearest
% neighbors.
%
% images is and m-by-n-by-p matrix consisting of p images that are m-by-n large.

%%
% Copyright 2004-2021 L. Martyn Klassen
%
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the "Software"),
% to deal in the Software without restriction, including without limitation
% the rights to use, copy, modify, merge, publish, distribute, sublicense,
% and/or sell copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:
%
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
% THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
% FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
% DEALINGS IN THE SOFTWARE.
%%

% Get the size of the image matrix
[m,n,p] = size(dirty);

% Set aside the required memory for the map construction
%summap = zeros(m+2, n+2, p);
%divmap = zeros(m+2, n+2);

% Generate the sum of point and nearest neighbors and count of number of points
% used - corners and edges do not use 9 points.
%for i = -1:1
%   for j = -1:1
%      summap(2+i:m+1+i, 2+j:m+1+j, :) = summap(2+i:m+1+i, 2+j:m+1+j, :) + dirty;
%      divmap(2+i:m+1+i, 2+j:m+1+j) = divmap(2+i:m+1+i, 2+j:m+1+j) + ones(m,n);
%   end
%end

% Set aside the required memory for the map construction
summap = zeros(m+3, n+2, p);
divmap = zeros(m+3, n+2);

% Generate the sum of point and nearest neighbors and count of number of points
% used - corners and edges do not use 9 points.
for i = 0:2
   for j = 0:2
      summap = summap + [zeros(j, n+2, p); zeros(m, i, p) dirty zeros(m, 2-i, p); zeros(3-j, n+2, p)];
      divmap = divmap + [zeros(j, n+2); zeros(m, i) ones(m,n) zeros(m, 2-i); zeros(3-j, n+2)];
   end
end

% Trim the extra edges from the maps
summap = summap(2:m+1,2:n+1,:);
divmap = divmap(2:m+1,2:n+1);

% Extend the divmap to the size of the image matrix
divmap2 = divmap - 1;
divmap2 = repmat(divmap2, [1 1 p]);
divmap = repmat(divmap, [1 1 p]);

% Compute the average of the nearest neighbors
fillmap = (summap - dirty)./divmap2;

% Compute a logical map indicating if the point is a spike
summap = dirty > tol.*summap./divmap;

% Construct new image of replacing spikes with nearest neighbor values
clean = dirty.*(~summap) + fillmap.*summap;

