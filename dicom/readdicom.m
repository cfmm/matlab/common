function series = readdicom(datapath, rescale, callback, recursive)

% SERIES = READDICOM(PATH, RESCALE, CALLBACK, RECURSIVE)
%
% Read DICOM data from PATH
% Reads all DICOM series from PATH and stores them as separate SERIES.  The
% data is rescaled according to the DICOM RescaleSlope and RescaleIntercept
% if the DICOM attribute exists for all instances in the series and RESCALE
% is true (default false). Only series that return true for
% CALLBACK(header), will be read (default CALLBACK always returns true).
% All dicom series will be read recursively from directory if RECURSIVE is
% true (default false);

%%
% Copyright 2020-2023 L. Martyn Klassen
%
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the "Software"),
% to deal in the Software without restriction, including without limitation
% the rights to use, copy, modify, merge, publish, distribute, sublicense,
% and/or sell copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:
%
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
% THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
% FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
% DEALINGS IN THE SOFTWARE.
%%

if nargin < 4
   recursive = false;
   if nargin < 3
      callback = @default_callback;
      if nargin < 2
         rescale = false;
      end
   end
end

if isempty(callback)
   callback = @default_callback;
end

series = local_readdicom(datapath, rescale, callback, recursive);
return


function series = local_readdicom(datapath, rescale, callback, recursive)

%% Get all the files in path
files = dir(datapath);

% Strip out . and ..
files = files(~or({files.name} == "." , {files.name} == ".."));

series = struct([]);

if isempty(files)
   return
end

data = struct([]);
rseries = struct([]);

series_number = {};
for i = 1:numel(files)
   if files(i).isdir
      if recursive
         rseries = [rseries local_readdicom([datapath filesep files(i).name], rescale, callback, recursive)];
      end
      continue;
   end

   % Make sure the file is a DICOM file
   try
      header = dicominfo([datapath filesep files(i).name]);
   catch ME
      if strcmp(ME.identifier, 'images:dicominfo:notDICOM')
         continue;
      end
      rethrow ME;
   end

   % Check that the file matches requested files
   if ~callback(header)
      continue;
   end

   idx = cellfun(@(x) strcmp(x, header.SeriesInstanceUID), series_number);
   dcm.data = dicomread(header);
   dcm.info = header;

   if ~any(idx)
      data(end+1).dcm = dcm;
      series_number{end+1} = header.SeriesInstanceUID;
   else
      data(idx).dcm(end+1) = dcm;
   end

end

% Sort by InstanceNumber followed by SOPInstanceUID
% This is to handle issues when series have repeating instance numbers
% within a series, e.g. uncombined coil images from Siemens MR.
% Is assumes the SOPInstanceUID will be assigned in monotonic increasing
% order. This is an empircal observation of Siemens datasets and may not
% always hold.
for i = 1:numel(data)
   try
      headers = [data(i).dcm.info];
   catch ME
      if strcmp(ME.identifier, 'MATLAB:catenate:structFieldBad')
         newME = MException('readdicom:header', ...
            'SeriesNumber %i has inconsitent DICOM headers', ...
            data(i).dcm(1).SeriesNumber);
         throw(newME);
      end
      rethrow(ME);
   end

   try
      instanceNumber = [headers.InstanceNumber];
      SOPInstanceUID = {headers.SOPInstanceUID};

      if isfield(header(1), 'Private_0051_100f')
         % Receivers image have identical standard DICOM headers
         % Need to use Siemens private headers to ensure the images
         % are in a predicatable order
         rx = {headers.Private_0051_100f};
         rx = cellfun(@(x) char(x(:).'), rx, 'UniformOutput', false);
         rx_string = regexp(rx, '\D*', 'match', 'once');
         rx_number = str2double(regexp(rx, '\d*', 'match', 'once'));
         sortfields = [num2cell(instanceNumber(:)) rx_string(:) num2cell(rx_number(:)) SOPInstanceUID(:)];
      else
         sortfields = [num2cell(instanceNumber(:)) SOPInstanceUID(:)];
      end

      [~,idx] = sortrows(sortfields);

      nInstances = numel(instanceNumber);
      nInstance = numel(unique(instanceNumber));
      nOther = nInstances / nInstance;

      if nOther ~= uint64(nOther)
         ME = MException('readdicom:InstancesPerSeries', ...
            'Number of unique instances (%i) is not factor of total images (%i) in series', ...
            nInstance, nInstances);
         throw(ME);
      end

      siz = size(data(i).dcm(1).data);

      if isfield(headers(1), 'NumberOfFrames')
         nFrames = headers(1).NumberOfFrames;
         nFramesActual = prod(siz(3:4)) / (nOther * nInstances);

         if nFrames ~= nFramesActual
            ME = MException('readdicom:NumberOfFrames', ...
               'NumberOfFrames (%i) is not reflected in the data (%i)', ...
               nFrames, nFramesActual);
            throw(ME);
         end
         siz(3:4) = [nOther, nInstance*nFrames];
      else
         siz(3:4) = [nOther, nInstance];
      end
      
      series(i).data = permute(reshape([data(i).dcm(idx).data], siz), [1 2 4 3]);
      series(i).info = permute(reshape(headers(idx), [nOther, nInstance]), [2 1]);

      % Slices and instance order do not always align
      sliceLocation = [series(i).info.SliceLocation];
      nSlice = numel(unique(sliceLocation));
      if mod(nInstance, nSlice) == 0 && ~issorted(sliceLocation, 'monotonic')
         % Assume that slice stack will come first, e.g. all slice for a
         % repetition come before (in instance number) a slice of the next
         % repetition. Also assume that the slice stack is first.

         % Compute the slice ordering, unique for every volume
         siz(3:5) = [nSlice nInstance/nSlice nOther];
         sliceLocation = reshape(sliceLocation, siz(3:5));

         % Only sort if slice order is not already monotonic and there are no
         % repeats in any of the slice stacks. This is to protect against
         % violation of the above assumptions
         if ~issorted(sliceLocation, 1, 'monotonic') && ...
               all(splitapply(@isunique, sliceLocation, 1:prod(siz(4:5))))
            [~,idx] = sort(sliceLocation);

            % Split out the slices from the other instances
            series(i).data = reshape(series(i).data, siz);
            series(i).info = reshape(series(i).info, siz(3:end));

            % Sort the volumes
            series(i).data = series(i).data(:,:,idx);
            series(i).info = series(i).info(idx);

            % Combine the slice with the other instances again
            siz(3:4) = [nInstance nOther];
            siz = siz(1:4);
            series(i).data = reshape(series(i).data, siz);
            series(i).info = reshape(series(i).info, siz(3:end));
         end
      end
   catch ME
      switch ME.identifier
         case 'MATLAB:nonExistentField'
         otherwise
            rethrow(ME)
      end
   end
end

if rescale
   for i = 1:numel(series)
      try
         header = series(i).info(1);
         if isfield(header, "NumberOfFrames")
            nImages = numel(series(i).info);
            nFrames = header.NumberOfFrames;
            try
               slope = zeros(nFrames, nImages);
               intercept = zeros(nFrames, nImages);
               for frame = 1:nFrames
                  slope(frame,:) = [getfield(series(i).info.PerFrameFunctionalGroupsSequence, sprintf('Item_%i', frame)).PixelValueTransformationSequence.Item_1.RescaleSlope];
                  intercept(frame,:) = [getfield(series(i).info.PerFrameFunctionalGroupsSequence, sprintf('Item_%i', frame)).PixelValueTransformationSequence.Item_1.RescaleIntercept];
               end
            catch error
               if ~strcmp(error.identifier, 'MATLAB:nonExistentField')
                  rethrow(error);
               end
               slope = [series(i).info.SharedFunctionalGroupsSequence.Item_1.PixelValueTransformationSequence.Item_1.RescaleSlope];
               intercept = [series(i).info.SharedFunctionalGroupsSequence.Item_1.PixelValueTransformationSequence.Item_1.RescaleIntercept];
            end
            shape = [1 1 size(slope)];
            slope = reshape(slope, shape);
            intercept = reshape(intercept, shape);
            siz = size(series(i).data);
            % Need to split the frames from the other arrays to apply
            % scaling
            series(i).data = reshape(reshape(double(series(i).data), [siz(1:2) nFrames nImages]) .* slope + intercept, siz);
         else
            slope = [series(i).info.RescaleSlope];
            intercept = [series(i).info.RescaleIntercept];
            siz = size(series(i).info);
            slope = reshape(slope, [1 1 siz]);
            intercept = reshape(intercept, [1 1 siz]);

            series(i).data = double(series(i).data) .* slope + intercept;
         end
      catch error
         if ~strcmp(error.identifier,'MATLAB:nonExistentField')
            rethrow(error);
         end
      end
   end
end

%% Combine the current and recursive studies
series = [series rseries];
return


function value = default_callback(header)
value = true;
return


function result = isunique(data)
result = numel(data) == numel(unique(data));
return

