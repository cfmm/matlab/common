% Unit tests for readdicom.m
% August 2020

% Copyright 2020-2021 L. Martyn Klassen
%
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the "Software"),
% to deal in the Software without restriction, including without limitation
% the rights to use, copy, modify, merge, publish, distribute, sublicense,
% and/or sell copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:
%
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
% THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
% FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
% DEALINGS IN THE SOFTWARE.
%%

%% Load sample data required as input to readdicom.m
datadir = [fileparts(mfilename('fullpath')) filesep];
dest = 'readdicom_testdata2';
importSampleData(datadir,'readdicom_testdata2.zip', dest, '19263325');

%% Test 1: reading against select DICOM files
data = readdicom([datadir dest], false, [], true);
assert( abs(mean(data.data(:)) - 42.1113) < 0.0001);

%% Test 2: read in @required_series
data = readdicom([datadir dest], false, @required_series, true);
assert( abs(mean(data.data(:)) - 42.1113) < 0.0001);

%% Test 3: Scaling series data
data = readdicom([datadir dest], true, [], true);
assert( abs(mean(data.data(:)) - 0.6501) < 0.0001);

%% Specific data series to read in
function value = required_series(header)
value = ...
   strcmp(header.Modality, "MR") && ...
   (strcmp(header.SeriesDescription, "Absolute B1 cfg=b1qa_abs.ipr abs B1"));
end
