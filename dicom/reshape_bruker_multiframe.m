function reshaped_series = reshape_bruker_multiframe(series,header)
% *************************************************************************
% FUNCTION:    reshaped_series = reshape_bruker_multiframe(series,header)
%
% DESCRIPTION: Sort multi-frame Bruker data into disparate dimensions:
%              [dim1 dim2 nSlices*(Mag/Phs maps) nTimePoints nRxCoils]. The
%              function can accept the output of 'readdicom', which will
%              have SERIES data as fields in a struct. Alternatively, the
%              single dataset can be passed as an input, SERIES, along with its
%              HEADER, as created from 'dicomread' and 'dicominfo'.
%
% INPUTS:      series - input data series as created from 'readdicom' or
%               'dicomread'
%              header - dicom header if using 'dicomread' and 'dicominfo'
%               to read in data (OPTIONAL - not used if using 'readdicom')
%
% OUTPUTS:     reshaped_series - Bruker image data reshaped to:
%               [dim1 dim2 nSlices*(Mag/Phs maps) nTimePoints nRxCoils]
%
% Created by:  Kyle M. Gilbert
%              March 2023
%% *************************************************************************
% Determine if the input data contains multiple series in a struct or a
% single matrix. If a single matrix, the header must be passed as an input.
if isstruct(series)
   nSeries = numel(series);
else
   nSeries = 1;
   if nargin < 2
      error('Must provide header as input');
   end
end

%% Loop through each series
for i = 1:nSeries

   % Determine if the input data contains multiple series in a struct
   if isstruct(series)
      header = series(i).info(1);
      data = series(i).data;
      if numel(series(i).info) > 1
         error('Unsupported mix of multiframe and individual instances');
      end
   else
      data = series;
   end

   % Determine the number of slices, time points, and if mag/phs maps are
   % provided (like in B1+ maps)
   for FrameNum = 1:size(data,3)
      frame = getfield(header.PerFrameFunctionalGroupsSequence, sprintf('Item_%i', FrameNum));

      % Keep track of the slice position, time point, and frame label for each frame
      SlicePos(FrameNum,:) = frame.PlanePositionSequence.Item_1.ImagePositionPatient;

      try % time point for time series data
         TimePoint(FrameNum) = frame.FrameContentSequence.Item_1.TemporalPositionIndex;
         if strcmp(header.PulseSequenceName,'Bruker:B1Map')
            TimePoint(FrameNum) = 1; % Since the B1 map isn't actually a time series
         end
      catch
         TimePoint(FrameNum) = 1;
      end

      try % Determine whether there is both phase and magnitude data
         FrameLabel{FrameNum} = frame.FrameContentSequence.Item_1.FrameLabel;
      catch
         FrameLabel{FrameNum} = 'Amplitude';
      end
   end

   % Convert to COMPLEX data
   sz = size(data);
   if strcmp(header.ComplexImageComponent,'MIXED')
      data = reshape(data, [sz(1) sz(2) sz(3)./2 2]);
      data = data(:,:,:,1) + 1i*data(:,:,:,2);
      nComponents = 2;
   else
      nComponents = 1;
   end

   % Reshape the data matrix: [dim1 dim2 nSlices*(Mag/Phs maps) nTimePoints nRxCoils]
   nSlices = max([length(unique(SlicePos(:,1))) length(unique(SlicePos(:,2))) length(unique(SlicePos(:,3)))]);
   nTimePoints = length(unique(TimePoint));
   nFrameLabels = length(unique(FrameLabel));
   nRxCoils = header.NumberOfFrames ./ (nTimePoints .* nSlices .* nFrameLabels .* nComponents);

   data = reshape(data, [sz(1) sz(2) nSlices*nFrameLabels nTimePoints nRxCoils]);

   % Create the final ouput
   if isstruct(series)
      reshaped_series(i).data = squeeze(data);
      reshaped_series(i).info = series(i).info;
   else
      reshaped_series = squeeze(data);
   end

   clear SlicePos TimePoint FrameLabel % necessary for multiple series to be read in correctly

end
