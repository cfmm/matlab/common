function extractprot(dicomfile, outputfile)

% Extract MrProt from DICOM file
%
% EXTRACTPROT(DICOMFILE, OUTPUTFILE)
%
% Read the MrProt from the DICOM file DICOMFILE and write to OUTPUTFILE

% Author: Martyn Klassen
% Created: 20200820

%%
% Copyright 2020-2021 L. Martyn Klassen
%
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the "Software"),
% to deal in the Software without restriction, including without limitation
% the rights to use, copy, modify, merge, publish, distribute, sublicense,
% and/or sell copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:
%
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
% THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
% FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
% DEALINGS IN THE SOFTWARE.
%%

dicomheader = dicominfo(dicomfile);
text = native2unicode(dicomheader.Private_0029_1020');
first = strfind(text, '### ASCCONV BEGIN');
last = strfind(text, '### ASCCONV END') + 18;
text = strrep(text(first:last), '""', '"');
fid = fopen(outputfile, 'wt');
fprintf(fid, '%s', text);
fclose(fid);

