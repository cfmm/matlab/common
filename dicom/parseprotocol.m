function prot = parseprotocol(dicomheader, asText)

%PARSEPROTOCOL reads the Siemens protocol from the DICOM header
%   X = PARSEPROTOCOL(H) reads the protocol X from the DICOM header H

%%
% Copyright 2019-2021 L. Martyn Klassen
%
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the "Software"),
% to deal in the Software without restriction, including without limitation
% the rights to use, copy, modify, merge, publish, distribute, sublicense,
% and/or sell copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:
%
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
% THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
% FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
% DEALINGS IN THE SOFTWARE.
%%

% Search for the Private tag
tag = 'Private_0029_1020';
names = fieldnames(dicomheader);
for index = 1:length(names)
   name = names{index};
   if strcmp(dicomheader.(name), 'SIEMENS CSA HEADER') && ...
         matches(name, 'Private_0029_' + digitsPattern(2) + 'xx_Creator')
      tag = strrep(names{index}, 'xx_Creator', '20');
      break
   end
end

text = native2unicode(dicomheader.(tag)');
first = strfind(text, '### ASCCONV BEGIN');
last = strfind(text, '### ASCCONV END') + 18;

if isempty(first) || isempty(last)
   error('DICOM header does not contain a protocol')
end

% Collapse the text to the first start and last end flags
% i.e. ignore all end flags the occur before a start flag and all start
% flags the occur after the last end flag
text = text(first(1):last(end));
first = strfind(text, '### ASCCONV BEGIN');
last = strfind(text, '### ASCCONV END') + 18;

if numel(last) ~= 1
   last = regexp(text, '### ASCCONV END ###');
end

if numel(last) ~= 1
   error('Found %d ### ASCCONV END flags', numel(last));
end

if numel(first) ~= 1
   first = regexp(text, '### ASCCONV BEGIN [^\n]* ###');
end

if numel(first) ~= 1
   error('Found %d ### ASCCONV BEGIN flags', numel(first));
end

text = text(first:last);

if nargin > 1 && ~isempty(asText) && asText
   prot = text;
   return
end
text = splitlines(text);

n = size(text,1);

% Skip the first line and last line
prot = struct();
for idx=2:n-1
   % Split the text on the equal sign
   data = split(text(idx), '=');

   % Strip whitespace around the '.' delimited structured parameter name
   % Divide the name into the hierarchical structure
   name = split(strtrim(data(1)), '.');

   % Combine the data using '=', incase a string contained an '='
   % Strip away leading/trailing whitespace
   value = strtrim(join(data(2:end), '='));

   % Ignore all __attribute__ values
   % Arrays are made dynamically and may be smaller than specified size
   if any(strcmp(name, '__attribute__'))
      continue
   end

   % Cleanup strings and convert to numbers if possible
   % Remove quotes arround strings
   original_value = value{1};
   value = strip(original_value, '"');
   if ~strcmp(value, original_value)
      value = {value};
   else
      % Try to convert the value to a number
      numValue = str2double(value);

      % Catch hexadecimals ending in e
      if isnan(numValue)
         [a,count,errmsg,nextindex] = sscanf(value,'%i',1);
         if count == 1 && isempty(errmsg) && nextindex > numel(value)
            numValue = a;
         end
      end

      if ~isnan(numValue)
         value = numValue;
      end
   end

   % Add the field to the protocol structure
   prot = addvalue(prot, name, value);
end

return


function prot = addvalue(prot, fields, value)

% Get the first field name
field = fields{1};
% Check if this the last field name
last = numel(fields) == 1;

% Check if the field has an array index
if field(end) == ']'
   % Get the field name and index
   idx = strfind(field, '[');
   name = field(1:idx-1);
   idx = uint32(str2double(field(idx+1:end-1))) + 1;
else
   name = field;
   idx = uint32(0);
end

if idx == 0
   % Unarrayed strings do not need to be cell arrays
   if iscell(value)
      value = value{1};
   end

   % Non-array field
   if last
      % Terminal field so assign value
      prot.(name) = value;

      % Parameters with zero value are not listed in protocol
      % Make the required real and imaginary to simplify array parsing
      if strcmp(name, 'dRe')
         if ~isfield(prot, 'dIm')
            prot.dIm = 0.0;
         end
      elseif strcmp(name, 'dIm')
         if ~isfield(prot, 'dRe')
            prot.dRe = 0.0;
         end
      end
   else
      % Non-terminal field, build the required structure
      if isfield(prot, name)
         % Append to an existing structure
         prot.(name) = addvalue(prot.(name), fields(2:end), value);
      else
         % Append to an empty structure
         prot.(name) = addvalue(struct(), fields(2:end), value);
      end
   end
elseif isfield(prot, name)
   % Arrayed field that already exists
   if idx == 1
      % First array value
      if last
         % Terminal field so assign value
         prot.(name) = value;
      else
         % Adding an array of structures, so append to existing
         prot.(name) = addvalue(prot.(name)(idx), fields(2:end), value);
      end
   else
      % Expanding array, i.e. not the first
      if last
         % Terminal so just assign the value at the new index
         try
            prot.(name)(idx) = value;
         catch exception
            rethrow(exception)
         end
      else
         % Get the current or first structure so a new structure has all
         % the same fields as the first item in the array
         if numel(prot.(name)) < idx
            element = prot.(name)(1);
         else
            element = prot.(name)(idx);
         end
         % Add the field to the structure
         newElement = addvalue(element, fields(2:end), value);
         try
            % Assign the structure to the proper indexed location
            prot.(name)(idx) = newElement;
         catch ME
            if idx > 1
               % First elements had fields with zero value that were not
               % included and need to be added.
               oldElements = prot.(name);
               fixNames = fieldnames(newElement);
               n = numel(oldElements);
               newElements(n+1) = newElement;
               for idxElement = 1:n
                  % Add missing fields with zero value to the first item
                  element = oldElements(idxElement);
                  for i = 1:numel(fixNames)
                     if ~isfield(element, fixNames{i})
                        element.(fixNames{i}) = 0;
                     end
                  end
                  newElements(idxElement) = element;
               end
               prot.(name) = newElements;
            else
               rethrow(ME)
            end
         end
      end
   end
else
   % New arrayed parameter
   if last
      % Terminal so just assign value
      try
         prot.(name)(idx) = value;
      catch exception
         rethrow(exception)
      end
   else
      % Add a new structure based on an empty structure
      prot.(name)(idx) = addvalue(struct(), fields(2:end), value);
   end
end
