function [differences]=comparedicomheader(hdr1, hdr2)
% DIFFERENCES = COMPARDICOMHEADER(HDR1, HDR2)
%
%   Create a cell array of differences between DICOM headers HDR1 and HDR2

%%
% Copyright 2020-2021 L. Martyn Klassen
%
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the "Software"),
% to deal in the Software without restriction, including without limitation
% the rights to use, copy, modify, merge, publish, distribute, sublicense,
% and/or sell copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:
%
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
% THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
% FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
% DEALINGS IN THE SOFTWARE.
%%

names1 = fieldnames(hdr1);
names2 = fieldnames(hdr2);

common = intersect(names1, names2);

unique1 = setdiff(names1, names2);
unique2 = setdiff(names2, names1);

count = 0;
for i = 1:numel(common)
   if isequal(hdr1.(common{i}),hdr2.(common{i}))~=1
      count = count + 1;
      differences(count,1)=common(i);
      differences(count,2)={hdr1.(common{i})};
      differences(count,3)={hdr2.(common{i})};
   end
end

for i = 1:numel(unique1)
   count = count + 1;
   differences(count,1)=unique1(i);
   differences(count,2)={hdr1.(unique1{i})};
   differences(count,3)={[]};
end

for i = 1:numel(unique2)
   count = count + 1;
   differences(count,1)=unique2(i);
   differences(count,2)={[]};
   differences(count,3)={hdr2.(unique2{i})};
end

