# CFMM Common MATLAB toolbox

MATLAB toolbox with commonly used functions

## Recommended git configuration

Trailing whitespaces should be avoided and git can help.

```shell
git config core.whitespace trailing-space,space-before-tab,tab-in-indent
git config apply.whitespace fix
```

## Editor setting

EditorConfig setting are set. Unfortunately Mathworks has not yet adopted this standard for configuring the builtin
editor. MATLAB editor should be configured:

1. Spaces not tabs for indentation
2. Indent 3 spaces
3. Trim trailing whitespace
4. Unix style linefeeds

